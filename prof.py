import urllib, requests

def read_text():
   access= open("prof.txt") #access the document
   contents = access.read()
   print(contents)
   access.close()
   prof_check(contents) #run function below

def prof_check(text_to_check):
    r = requests.get("https://www.purgomalum.com/service/containsprofanity?text="+text_to_check) #append contents of prof.txt to url
    prof_results = r.text #display results
    if "true" in prof_results:
        print("Careful, this text has bad words")
    elif "false" in prof_results:
        print("Safe, no bad words here.")
    else:
        print("Error reading the text, please try again.")
read_text()
