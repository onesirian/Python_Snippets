###### POMODORO ##############
import time
import webbrowser

breaks = 3
break_count = 0
##  Input number of seconds below. 25 minutes = 1500
wait = 1800
print("Now starting at "+time.ctime())
while(break_count < breaks):
    time.sleep(wait)
    webbrowser.open("https://www.youtube.com/watch?v=YwANQb4phH0")
    break_count +=1
####LaunchCode LC101 Notes and Code Snippets#############
#basic assignment
name = raw_input('What is your name?\n')
print 'Hi,', name

# Print the highest number in a list
numbers = [-500, 5, 888, -8, -133, 42, 6]
highnum = -999
for number in numbers:
  if number >= highnum:
  highnum = number
print("The highest number is", highnum)

#sumto function
def sum_to(n):
    sum = 0
    for num in range(1, n+1):
        sum = sum + num
    return sum
print(sum_to(4))
print(sum_to(1000))

#while quick and easy
def sum_to(n):
    """ Return the sum of 1+2+3 ... n """
    sum = 0
    num = 0
    while num <= n:
        sum = sum + num
        num = num + 1
    return sum
print(sum_to(4))
print(sum_to(1000))

###############while with a break###################
def sum_to(n):
    """ Return the sum of 1+2+3 ... n, up to 100"""
    sum = 0
    num = 0
    while num <= n:
        sum = sum + num
        num = num + 1
        if num > 100:
            break
    return sum
def main():
    print(sum_to(4))
    print(sum_to(100))
    print(sum_to(500))
if __name__ == "__main__":
    main()
############ While with a continue #################
def sum_to(n):
    """ Return the sum of odd numbers from 1 to n """
    sum = 0
    num = 0
    while num <= n:
        if num % 2 == 0:
            num = num + 1
            continue
        sum = sum + num
        num = num + 1
    return sum
def main():
    print(sum_to(4))
    print(sum_to(100))
if __name__ == "__main__":
    main()
    ######Fizz buzz with a twist#################
for number in range(1, 41):
  if number % 3 == 0:
  print(number, "Hip")
elif number % 7 == 0:
  print(number, "Hop")

# greet some friends
def greet_coworkers(message):
  more_people = input("Is there another person to greet? Type y for yes and n for no.")
more_people = more_people.lower()

while (more_people == "y"):
  name = input("What is your coworker's name?")
print(message, name + "!")# instead of using a hard coded message, we use the passed in one
more_people = input("Is there another person to greet? Type y for yes and n for no.")
more_people = more_people.lower()

greet_coworkers("Good morning")# do some work till lunch time
  print("It's lunchtime!")
greet_coworkers("See you after lunch")# return from lunch
print("Back to work...")# work till end of day
print("Finally, working day is through!")
greet_coworkers("Goodbye")# -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

  #square a number
def square(number):
  return number * number

feet_in_room = 8
cost_of_flooring = 15 * square(feet_in_room)
print("It will cost $" + str(cost_of_flooring) + " to put hardwood flooring in your room.")

# Calculate interest compounded monthly-- -- -- -- -- -- -- -- -- -- -- -- -- -- -
def calculate_interest(principal):
  return principal * (1 + .04 / 12) ** 12# prompt the user
for initial
deposit = float(input("What amount of money did you deposit? Enter the number with no $ nor ."))# print the result
print(calculate_interest(deposit))

# Find mean average
numbers = [2, 4, 6, 8]
number_items = len(numbers)
sum_of_numbers = 0# loop through the list adding up all the numbers
for number in numbers:     sum_of_numbers += number
avg = sum_of_numbers / number_items
print(avg)

# Find average using
function
def calculate_average(runs, games):
  sum_of_runs = 0
for run in runs:
  sum_of_runs = sum_of_runs + run
avg = sum_of_runs / games
return avg
runs_list = [5, 7, 3, 1, 4]
num_games = len(runs_list)
print(calculate_average(runs_list, num_games))

# Find percentage
total_students = int(input("How many students are in the class?"))
passing_students = int(input("How many students passed this test?"))
def find_percentage():
  return passing_students / total_students
percentage = int(find_percentage() * 100)
print("The percentage of students who passed is " + str(percentage) + "%")

######Print 2 power tables#############
print("n", '\t', "2**n")     #table column headings
print("---", '\t', "-----")
for x in range(10):        # generate values for columns
    print(x, '\t', 2 ** x)
#######################################
# area and perimeter
def area(length, width):
  return length * width
def perimeter(length, width):
  return 2 * (length + width)
print("The area of this rectangle is", area(10, 4), "square feet.")
print("The perimeter of this rectangle is", perimeter(10, 4), "feet.")
print("The area of this square is", area(8, 8), "square feet.")
print("The perimeter of this square is", perimeter(8, 8), "feet.")

# find diameter from circumference
def calculate_diameter(circumference):
  return circumference / 3.14
circ = int(input("Type the circumference. Do not include units of measurement"))
diameter = calculate_diameter(circ)
print(diameter)

# area from radius(circle)
def find_area(radius):
  area = 3.14 * (radius) ** 2
return area
user_input = int(input("Type in your radius"))
print(find_area(user_input), "sq ft is your total area")

# string replace method#(old, new, count)
name1 = "abcdefgabcdefg"
name2 = name1.replace("c", "x", "1")
                    
# find a character full_name = "Lawrence Lockhart"
initial = "e"
first_e = full_name.find(initial) print(first_e)

# find url string on a webpage page = contents of a webpage start_link = page.find('<a href=') start_quote = page.find('"', start_link) end_quote = page.find('"', start_quote + 1) url = page[start_quote + 1: end_quote] print(url) print("Welcome to the Loop Hole!") print("Today's Manager's Special is: Crunch Jelly: A traditional jelly donut in which the jelly filling is made entirely of Capn Crunch Oops! All Berries") qty = float(input("How many would you like")) price = float(input("How much would you like to pay")) last_line = "Ok, let me prepare that for you....After tax, your total is: $"
print(last_line.strip(), (qty * price), sep = '') print("Thank you for snacking! Loop back around here soon!")# -- -- -- -- -- - GLOSSARY-- -- -- -- -- -- -- -- -- -
activecode A unique interpreter environment that allows Python to be executed from within a web browser.algorithm A general step by step process for solving a problem.byte code An intermediate language between source code and object code.Many modern languages first compile source code into byte code and then interpret the byte code with a program called a virtual machine.codelens An interactive environment that allows the user to control the step by step execution of a Python program.comment Information in a program that is meant
for other programmers(or anyone reading the source code) and has no effect on the execution of the program.compile To translate a program written in a high - level language into a low - level language all at once, in preparation
for later execution.executable Another name
for object code that is ready to be executed.formal language Any one of the languages that people have designed
for specific purposes, such as representing mathematical ideas or computer programs; all programming languages are formal languages.high - level language A programming language like Python that is designed to be easy
for humans to read and write.interpret To execute a program in a high - level language by translating it one line at a time.low - level language A programming language that is designed to be easy
for a computer to execute; also called machine language or assembly language.natural language Any one of the languages that people speak that evolved naturally.object code The output of the compiler after it translates the program.portability A property of a program that can run on more than one kind of computer.print
function A
function used in a program or script that causes the Python interpreter to display a value on its output device.problem solving The process of formulating a problem, finding a solution, and expressing the solution.program A sequence of instructions that specifies to a computer actions and computations to be performed.programming language A formal notation
for representing solutions.Python shell An interactive user interface to the Python interpreter.The user of a Python shell types commands at the prompt( >>> ), and presses the
return key to send these commands immediately to the interpreter
for processing.shell mode A style of using Python where we type expressions at the command prompt, and the results are shown immediately.Contrast with source code, and see the entry under Python shell.source code A program, stored in a file, in a high - level language before being compiled or interpreted.token One of the basic elements of the syntactic structure of a program, analogous to a word in a natural language.assignment statement A statement that assigns a value to a name(variable).To the left of the assignment operator, = , is a name.To the right of the assignment token is an expression which is evaluated by the Python interpreter and then assigned to the name.The difference between the left and right hand sides of the assignment statement is often confusing to new programmers.In the following assignment:

n = n + 1 n plays a very different role on each side of the = .On the right it is a value and makes up part of the expression which will be evaluated by the Python interpreter before assigning it to the name on the left.

#assignment token = is Python’ s assignment token, which should not be confused with the mathematical operator using the same symbol.class see data type below comment Information in a program that is meant
    for other programmers(or anyone reading the source code) and has no effect on the execution of the program.data type A set of values.The type of a value determines how it can be used in expressions.So far, the types you have seen are integers(int), floating - point numbers(float), and strings(str).decrement Decrease by 1. evaluate To simplify an expression by performing the operations in order to yield a single value.
    expression A combination of operators and operands(variables and values) that represents a single result value.Expressions are evaluated to give that result.float A Python data type which stores floating - point numbers.Floating - point numbers are stored internally in two parts: a base and an exponent.When printed in the standard format, they look like decimal numbers.Beware of rounding errors when you use floats, and remember that they are only approximate values.increment Increase by 1. initialization( of a variable) To initialize a variable is to give it an initial value.Since in Python variables don’ t exist until they are assigned values, they are initialized when they are created.In other programming languages this is not the
  case, and variables can be created without being initialized, in which
  case they have either
  default or garbage values.int A Python data type that holds positive and negative whole numbers.integer division An operation that divides one integer by another and yields an integer.Integer division yields only the whole number of times that the numerator is divisible by the denominator and discards any remainder.keyword A reserved word that is used by the compiler to parse program; you cannot use keywords like
  if, def, and
  while as variable names.modulus operator Also called remainder operator or integer remainder operator.Gives the remainder after performing integer division.object Also known as a data object(or data value).The fundamental things that programs are designed to manipulate(or that programmers ask to do things
    for
    them).operand One of the values on which an operator operates.operator A special symbol that represents a simple computation like addition, multiplication, or string concatenation.prompt string Used during interactive input to provide the user with hints as to what type of value to enter.reference diagram A picture showing a variable with an arrow pointing to the value(object) that the variable refers to.See also state snapshot.rules of precedence The set of rules governing the order in which expressions involving multiple operators and operands are evaluated.state snapshot A graphical representation of a set of variables and the values to which they refer, taken at a particular instant during the program’ s execution.statement An instruction that the Python interpreter can execute.So far we have only seen the assignment statement, but we will soon meet the
  import statement and the
  for statement.str A Python data type that holds a string of characters.type conversion
  function A
  function that can convert a data value from one type to another.value A number or string(or other things to be named later) that can be stored in a variable or computed in an expression.variable A name that refers to a value.variable name A name given to a variable.Variable names in Python consist of a sequence of letters(a..z, A..Z, and _) and digits(0. .9) that begins with a letter.In best programming practice, variable names should be chosen so that they describe their use in the program, making the program self documenting.bug An error in a program.debugging The process of finding and removing any of the three kinds of programming errors.exception Another name
  for a runtime error.parse To examine a program and analyze the syntactic structure.runtime error An error that does not occur until the program has started to execute but that prevents the program from continuing.semantic error An error in a program that makes it do something other than what the programmer intended.
  semantics The meaning of a program.syntax The structure of a program.syntax error An error in a program that makes it impossible to parse— and therefore impossible to interpret.#-- -- -- -- -- --Time Counter Thingy-- -- -- -- -- -- -- -
  str_seconds = input("Please enter the number of seconds you wish to convert") total_secs = int(str_seconds) hours = total_secs // 3600
  secs_still_remaining = total_secs % 3600 minutes = secs_still_remaining // 60
  secs_finally_remaining = secs_still_remaining % 60 print("Hrs=", hours, "mins=", minutes, "secs=", secs_finally_remaining)# -- -- - When does the alarm go off-- -- -- -- -
  raw_time = input("What time is it? ") wait = input("How long until the alarm? ") fixraw_time = int(raw_time) fix_wait = int(wait) newtime = (fixraw_time + fix_wait) % 24 print("The clock will go off at", newtime)# += is the incrementer

  #-- -- -- -- --Move that turtle-- -- -- -- -- -- -- -- -- --
  import turtle wn = turtle.Screen()# Set up the window and its attributes wn.bgcolor("lightgreen") tess = turtle.Turtle()# create tess and set some attributes tess.color("hotpink") tess.pensize(5) alex = turtle.Turtle()# create alex tess.forward(80)# Let tess draw an equilateral triangle tess.left(120) tess.forward(80) tess.left(120) tess.forward(80) tess.left(120)# complete the triangle tess.right(180)# turn tess around tess.forward(80)# move her away from the origin alex.forward(50)# make alex draw a square alex.left(90) alex.forward(50) alex.left(90) alex.forward(50) alex.left(90) alex.forward(50) alex.left(90) wn.exitonclick()

  # Turtle drawing square using a loop(iteration) import turtle# set up alex wn = turtle.Screen() alex = turtle.Turtle() for i in [0, 1, 2, 3]:
    #repeat four times alex.forward(50) alex.left(90) wn.exitonclick()

    # Now do it and change the color
    import turtle# set up alex wn = turtle.Screen() alex = turtle.Turtle() for a_color in ["yellow", "red", "purple", "blue"]:
    alex.color(a_color) alex.forward(50) alex.left(90) wn.exitonclick()

    # range
    for i in range(5) do something# Expression below print all even from 0 to 20 but NOT including 20
    for i in range(0, 20, 2)# range(start, stop, step) print(i)# Constructor - special class method that creates an object# Turtle methods can use negative angles and distances# turtle.up() and turtle.penup() pulls the pen up# turtle.down() and turtle.pendown() puts the pen back down# turtles can have shapes alex.shape(arrow) alex.shape(blank) alex.shape(circle) alex.shape(classic) alex.shape(square) alex.shape(triangle) alex.shape(turtle)# turtles can have speed alex.speed(10) and can stamp their footprint alex.(stamp)# drawing program:
    import turtle wn = turtle.Screen() wn.bgcolor("lightgreen") tess = turtle.Turtle() tess.color("blue") tess.shape("turtle") print(range(5, 60, 2)) tess.up()# this is new
    for size in range(5, 60, 2): #start with size = 5 and grow by 2 tess.stamp()# leave an impression on the canvas tess.forward(size)# move tess along tess.right(24)# and turn her wn.exitonclick()

    # Summary of Turtle Methods Method Parameters Description Turtle None Creates and returns a new turtle object forward distance Moves the turtle forward backward distance Moves the turtle backward right angle Turns the turtle clockwise left angle Turns the turtle counter clockwise up None Picks up the turtles tail down None Puts down the turtles tail color color name Changes the color of the turtle’ s tail fillcolor color name Changes the color of the turtle will use to fill a polygon heading None Returns the current heading position None Returns the current position goto x, y Move the turtle to position x, y begin_fill None Remember the starting point
    for a filled polygon end_fill None Close the polygon and fill with the current fill color dot None Leave a dot at the current position stamp None Leaves an impression of a turtle shape at the current location shape shapename Should be‘ arrow’, ‘classic’, ‘turtle’, or‘ circle’# A MODULE is a file containing Python definitions and statements intended
    for use in other Python programs.#There are many Python modules that come with Python as a part of the standard library.#import creates a name that references a particular module# we use dot notation to utilize something contained IN the module

    # Random moduel and a dice
    throw import random prob = random.random() print(prob) diceThrow = random.randrange(1, 7)# return an int, one of 1, 2, 3, 4, 5, 6
    print(diceThrow)# Doing the drunk walk
    import turtle opie = turtle.Turtle() turn_a = [160, -43, 270, -97, -43, 200, -940, 17, -86]
    for turn in turn_a:
    opie.forward(100) opie.right(turn)

    # Drunk walk full version
    import turtle wn = turtle.Screen() lovelace = turtle.Turtle()# move the turtle forward a little so that the whole path fits on the screen lovelace.penup() lovelace.forward(60)# now draw the drunk pirate 's path
    lovelace.pendown() for angle in [160, -43, 270, -97, -43, 200, -940, 17, -86]: #we use.left() so that positive angles are counter - clockwise# and negative angles are clockwise lovelace.left(angle) lovelace.forward(100) wn.exitonclick()# Draw a star - my version
    import turtle wn = turtle.Screen() opie = turtle.Turtle() opie.left(60) opie.forward(100) opie.right(120) opie.forward(100) opie.right(145) opie.forward(140) opie.right(155) opie.forward(150) opie.right(155) opie.forward(145) wn.exitonclick()

    # Draw a star - book version
    import turtle 
    turing = turtle.Turtle() 
    for i in range(5):
      turing.forward(110) 
      turing.left(216)
    # Race the turtles###########################
    import turtle
    import random 
    wn = turtle.Screen()
    fred = turtle.Turtle() 
    fred.shape("turtle") 
    fred.color("green")
    dred = turtle.Turtle() 
    dred.shape("turtle") 
    dred.color("red")
    dred.up() 
    dred.left(90) 
    dred.forward(50) 
    dred.right(90) 
    dred.down() 
    dred.stamp() 
    dred.forward((random.random()) * 200)
    fred.stamp() 
    fred.forward((random.random()) * 200)
    # Draw a line and star with a function
    import turtle 
    def draw_line(length, angle):
      mike = turtle.Turtle() 
      mike.left(angle) 
      mike.forward(length / 2) 
      mike.forward(-length) 
      mike.forward(length / 2) 
    def star(lines):
      for angle in range(0, 180, int(180 / lines)):
      draw_line(200, angle) 
    star(10)
#### Program to draw the Wagon Wheel######## #33
import turtle
wn = turtle.Screen() myguy = turtle.Turtle() wn.bgcolor("Green") myguy.color("Blue") myguy.speed(10) for i in range(6):
    for i in range(4):
    for i in range(4):
    myguy.forward(60) myguy.left(90) myguy.left(90) myguy.left(15)### Alternate Method using
    function
    import turtle wm = turtle.Screen() wm.bgcolor("green") dude = turtle.Turtle() dude.color('blue') dude.speed('20')

    def drawsquare(x, size):
    for i in range(4):
    x.forward(size) x.left(90)
######## Remove the Red #############
import image
img = image.Image("luther.jpg")
win = image.ImageWin(img.getWidth(),img.getHeight())
for col in range(img.getWidth()):
    for row in range(img.getHeight()):
        p = img.getPixel(col,row)
        new_pixel = image.Pixel(0,p.getGreen(),p.getBlue())
        img.setPixel(col,row,new_pixel)

img.draw(win)
win.exitonclick()
######## Blur the Image #############
import image
import sys
import random

img = image.Image("luther.jpg")
new_img = image.EmptyImage(img.getWidth(), img.getHeight())
win = image.ImageWin(img.getWidth(), img.getHeight())

for col in range(1, img.getWidth() - 1):
    for row in range(1, img.getHeight() - 1):
        
        random_x = random.randrange(-1,2)
        random_y = random.randrange(-1,2)
        random_pixel = img.getPixel(col + random_y, row + random_x)
      
        new_img.setPixel(col,row,random_pixel)

new_img.draw(win)
win.exitonclick()
################################################
####### Reverse image color ############
import image
img = image.Image("luther.jpg")
win = image.ImageWin(img.getWidth(), img.getHeight())
img.draw(win)
img.setDelay(1,5000)   # setDelay(0) turns off animation
for row in range(img.getHeight()):
    for col in range(img.getWidth()):
        p = img.getPixel(col, row)
        new_red = 255 - p.getRed()
        new_green = 255 - p.getGreen()
        new_blue = 255 - p.getBlue()
        new_pixel = image.Pixel(new_red, new_green, new_blue)
        img.setPixel(col, row, new_pixel)
img.draw(win)
win.exitonclick()
##############Draw multi-colored squares#############
import turtle
def draw_multicolor_square(t, sz):
    """Make turtle t draw a multi-colour square of sz."""
    for i in ['red','purple','hotpink','blue']:
        t.color(i)
        t.forward(sz)
        t.left(90)
wn = turtle.Screen()             # Set up the window and its attributes
wn.bgcolor("lightgreen")
tess = turtle.Turtle()           # create tess and set some attributes
tess.pensize(3)
size = 20                        # size of the smallest square
for i in range(15):
    draw_multicolor_square(tess, size)
    size = size + 10             # increase the size for next time
    tess.forward(10)             # move tess along a little
    tess.right(18)               # and give her some extra turn
wn.exitonclick()
############################
                      
    for _ in range(20):
    dude.left(20) drawsquare(dude, 100)
    # another version##############
    import turtle wn = turtle.Screen() myguy = turtle.Turtle() wn.bgcolor("Green") myguy.color("Blue") myguy.speed(20) for turn in range(20):
    for i in range(4):
    myguy.forward(60) myguy.left(90) myguy.left(20)
    ########## #40-89 range problem###########
    # For each click variable, calculate the temperature and print def find_temp(clicks):
    statement = "The temperature is"
    fin_stop = clicks + 40# add the clicks to the base of 40
    if (fin_stop < 90 and fin_stop > 39): #range 1: 40 - 89
    print((statement), (fin_stop)) elif(fin_stop > 90): #range 2: 90 +
    print((statement), ((clicks % 50) + 40))
    else :#range 3: Less than 40
    print((statement), ((clicks % 49) + 41)) click_1 = 0 find_temp(click_1) click_2 = 49 find_temp(click_2) click_3 = 74 find_temp(click_3) click_4 = 51 find_temp(click_4) click_5 = -1 find_temp(click_5) click_6 = 200 find_temp(click_6)# Draw select turtle select size###########
    import turtle def draw_square(t, sz):
    for i in range(4):
    t.forward(sz) t.left(90) wn = turtle.Screen()
                      # Set up the window and its attributes wn.bgcolor("lightgreen") alex = turtle.Turtle()# create alex draw_square(alex, 50)# Call the
    function to draw the square passing the actual turtle and the actual side size wn.exitonclick()# Draw multi - colored squares
#### Grades and Class Average ####
def course_grader(test_scores):
    sum = 0
    for score in test_scores:
        sum = sum + score
        avg = sum/len(test_scores)
    
    if avg >= 70 and sorted(test_scores)[0] >= 50:
        return "pass"
    else:
        return "fail"
####################################
def square(x):
    running_total = 0          # initialize the accumulator!
    for counter in range(x):
        running_total = running_total + x

    return running_total
num = 10
result = square(num)
print("The result of", num, "squared is", result)
####Squares on Squares#####
def square(i):
    j = i * i
    return j
def sum_of_squares(x, y, z):
    a = square(x)
    b = square(y)
    c = square(z)
    return a + b + c
num_1 = -5
num_2 = 2
num_3 = 10
result = sum_of_squares(num_1, num_2, num_3)
print(result)
num_4 = -2
num_5 = 7
num_6 = 9
result_2 = sum_of_squares(num_4, num_5, num_6)
print(result_2)
###Drawing squares and rectangles###
import turtle
def draw_rectangle(t, w, h):
    """Get turtle t to draw a rectangle of width w and height h."""
    for i in range(2):
        t.forward(w)
        t.left(90)
        t.forward(h)
        t.left(90)
def draw_square(t, sz):        # a new version of draw_square
    draw_rectangle(t, sz, sz)
wn = turtle.Screen()             # Set up the window
wn.bgcolor("lightgreen")
tess = turtle.Turtle()           # create tess
draw_square(tess, 50)
wn.exitonclick()
                      ###########Turtle square with functions***********

import turtle
def draw_square(t, sz):
    """Make turtle t draw a square of with side sz."""
    for i in range(4):
        t.forward(sz)
        t.left(90)
def main():                      # Define the main function
    wn = turtle.Screen()         # Set up the window and its attributes
    wn.bgcolor("lightgreen")
    alex = turtle.Turtle()       # create alex
    draw_square(alex, 50)         # Call the function to draw the square
    wn.exitonclick()
main()                           # Invoke the main function
########## Pass or Fail ############3
def course_grader(test_scores):
    sum = 0
    for score in test_scores:
        sum = sum + score
        avg = sum/len(test_scores)
    
    if avg >= 70 and sorted(test_scores)[0] >= 50:
        return "pass"
    else:
        return "fail"
        
def main():
    print(course_grader([100,75,45]))     # "fail"
    print(course_grader([100,70,85]))     # "pass"
    print(course_grader([80,60,60]))      # "fail"
    print(course_grader([80,80,90,30,80]))  # "fail"
    print(course_grader([70,70,70,70,70]))  # "pass"

if __name__ == "__main__":
    main()
##############Draw Bar Graph#############
import turtle
def draw_bar(t, height):
    """ Get turtle t to draw one bar, of height. """
    t.begin_fill()               # start filling this shape
    t.left(90)
    t.forward(height)
    t.write(str(height))
    t.right(90)
    t.forward(40)
    t.right(90)
    t.forward(height)
    t.left(90)
    t.end_fill()                 # stop filling this shape
def main():
    data = [48, 117, 200, 240, 160, 260, 220]
    max_height = max(data)
    num_bars = len(data)
    border = 10
    wn = turtle.Screen()             # Set up the window and its attributes
    wn.setworldcoordinates(0-border, 0-border, 40 * num_bars + border, max_height + border)
    wn.bgcolor("lightgreen")
    tess = turtle.Turtle()           # create tess and set some attributes
    tess.color("blue")
    tess.fillcolor("red")
    tess.pensize(3)
    for x in data:
        draw_bar(tess, x)
    wn.exitonclick()
if __name__ == "__main__":
    main()
####Miles traveled and reimbursement#####
def daily_miles_traveled(before, after, days):
    total_miles = after - before
    average_miles = total_miles / days
    return average_miles
def reimbursement(miles, pay, extra_pay):
    mileage = miles * pay
    return mileage + extra_pay
def main():
    daily_miles = daily_miles_traveled(1000.0, 1500.5, 5)
    per_mile_pay = .50
    per_diem = 100
    daily_reimbursement = reimbursement(daily_miles, per_mile_pay, per_diem)
    print("Amount you should be reimbursed per day:", daily_reimbursement)
if __name__ == "__main__":
    main()
#######Chap 5 exercise (squares) #####################
import turtle
def draw_square(t, sz):
    """Get turtle t to draw a square with sz side"""
    for i in range(4):
        t.forward(sz)
        t.left(90)
def main():
    wn = turtle.Screen()
    wn.bgcolor("lightgreen")
    alex = turtle.Turtle()
    alex.color("pink")
    alex.speed(100)
    alex.penup()
    alex.forward(-50)
    alex.pendown()
    for j in range(5):
        draw_square(alex,20)
        alex.penup()
        alex.forward(40)
        alex.pendown()
    wn.exitonclick()
if __name__ == "__main__":
    main()
######Concentric Squares#############
import turtle
def draw_square(t,sz):
    for i in range(4):
        t.forward(sz)
        t.left(90)
def main():
    nemo = turtle.Turtle()
    wn = turtle.Screen()
    wn.bgcolor("lightgreen")
    nemo.color("pink")
    nemo.pensize(3)
    nemo.speed(20)
    for j in range(6):
        nemo.penup()
        nemo.backward(10)
        nemo.right(90)
        nemo.forward(10)
        nemo.left(90)
        nemo.pendown()
        draw_square(nemo,(20*j))
main()
##############Random TURTLE Walk#################
import random
import turtle

def is_in_screen(screen, t):
    left_bound = - screen.window_width() / 2
    right_bound = screen.window_width() / 2
    top_bound = screen.window_height() / 2
    bottom_bound = -screen.window_height() / 2

    turtle_x = t.xcor()
    turtle_y = t.ycor()

    still_in = True
    if turtle_x > right_bound or turtle_x < left_bound:
        still_in = False
    if turtle_y > top_bound or turtle_y < bottom_bound:
        still_in = False

    return still_in

def main():
    julia = turtle.Turtle()
    screen = turtle.Screen()

    julia.shape('turtle')
    while is_in_screen(screen, julia):
        coin = random.randrange(0, 2)
        if coin == 0:
            julia.left(90)
        else:
            julia.right(90)

        julia.forward(50)

    screen.exitonclick()

if __name__ == "__main__":
    main()

####Truth Tables####
p	    q	    p and q
True	True	True
True	False	False
False	True	False
False	False	False
#####################
p	    q	    p or q
True	True	True
True	False	True
False	True	True
False	False	False
#####################
p	    not p
True	False
False	True
##########Order of Operations###########
Level	Category	Operators
7(high)	exponent	**
6	     multiplication	*,/,//,%
5	     addition	+,-
4	     relational	==,!=,<=,>=,>,<
3      logical	not
2	     logical	and
1(low)logical	  or
#######Boolean function########
def is_divisible(x, y):
    if x % y == 0:
        result = True
    else:
        result = False
    return result

def main():
    if is_divisible(10, 5):
        print("That works")
    else:
        print("Those values are no good")

if __name__ == "__main__":
    main()
########## RGB Chart #############
Color	Red	Green	Blue
Red	  255	  0	    0
Green	0	    255	  0
Blue	0	    0	  255
White	255 	255	255
Black	0	    0	   0
Yellow	255	255	  0
Magenta	255	0	  255
###### Pixel Object ###############
Method Name	Example	Explanation
Pixel(r,g,b)	Pixel(20,100,50)	Create a new pixel with 20 red, 100 green, and 50 blue.
getRed()	r = p.getRed()	Return the red component intensity.
getGreen()	r = p.getGreen()	Return the green component intensity.
getBlue()	r = p.getBlue()	Return the blue component intensity.
setRed()	p.setRed(100)	Set the red component intensity to 100.
setGreen()	p.setGreen(45)	Set the green component intensity to 45.
setBlue()	p.setBlue(156)	Set the blue component intensity to 156.
####### Image Object #############
Method Name	Example	Explanation
Image(filename)	img = image.Image(“cy.png”)	Create an Image object from the file cy.png.
EmptyImage()	img = image.EmptyImage(100,200)	Create an Image object that has all “White” pixels
getWidth()	w = img.getWidth()	Return the width of the image in pixels.
getHeight()	h = img.getHeight()	Return the height of the image in pixels.
getPixel(col,row)	p = img.getPixel(35,86)	Return the pixel at column 35, row 86.
setPixel(col,row,p)	img.setPixel(100,50,mp)	Set the pixel at column 100, row 50 to be mp.
######## Basic pixelxpixel iteration ##########
for row in range(img.getHeight()):
    for col in range(img.getWidth()):
        # do something with the pixel at position (col,row)
#####Bar chart#####################
import turtle
def draw_bar(t,height):
    if height > 200:
        t.fillcolor("red")
    elif height < 100:
        t.fillcolor("yellow")
    elif height >= 0:
        t.fillcolor("green")
    else:
        t.write(str(height))
    t.begin_fill()               # start filling this shape
    t.left(90)
    t.forward(height)
    t.write(str(height))
    t.right(90)
    t.forward(40)
    t.right(90)
    t.forward(height)
    t.left(90)
    t.end_fill()                 # stop filling this shape

def main():
    data = [-48, 117, 200, 240, 160, 260, 220]
    max_height = max(data)
    num_bars = len(data)
    border = 10

    wn = turtle.Screen()             # Set up the window and its attributes
    wn.setworldcoordinates(0-border, 0-border, 40 * num_bars + border, max_height + border)
    wn.bgcolor("lightgreen")
    tess = turtle.Turtle()           # create tess and set some attributes
    tess.color("blue")
    tess.fillcolor("red")
    tess.pensize(3)
    for x in data:
        draw_bar(tess, x)
    wn.exitonclick()
#####PRINT N-X-N HASH MARKS#######
def square(n):
    for i in range(n):
        for i in range(n):
            print("#",end="")
        print('\n')      
def main():
    square(5)   
if __name__ == "__main__":
    main()
if __name__ == "__main__":
    main()
###Print the Grade########
grade = int(input("What was your grade?"))
if grade >= 90:
    print("A")
elif grade >= 80:
    print("B")
elif grade >= 70:
    print("C")
elif grade >= 60:
    print("D")
else:
    print("You failed")
###### Count the Characters ################
def count(text, char):
    letter_count = 0
    for c in text:
        if c == char:
            letter_count = letter_count + 1
    return letter_count

def main():
    print(count("banana","a"))

if __name__ == "__main__":
    main()
###### Reverse the String ##################
from test import testEqual
def reverse(text):
    new_text=""
    for i in range((len(text)-1),-1,-1):  #the middle -1 is non-inclusive so it stops at index 0
        new_text += text[i]
    return new_text
print(reverse("Lawrence"))
###### Remove the Vowels ###################
def remove_vowels(s):
    vowels = "aeiouAEIOU"
    no_vowels = ""
    for char in s:
        if char not in vowels:
            no_vowels = no_vowels + char
    return no_vowels

def main():
    print(remove_vowels("compsci"))
    print(remove_vowels("aAbEefIijOopUus"))

if __name__ == "__main__":
    main()
######Letter and Word Analysis #############
import string
def analyze_text(text):
    text_alpha = ""
    for ch in text:
        if ch.isalpha() == True:
            text_alpha += ch
    new_text = text_alpha.lower()
    ecount = 0
    textlen = len(new_text)
    for e in new_text:
        if e.isalpha() and e == "e":
            ecount += 1
        else:
            ecount = ecount
    return ("The text contains {0} alphabetic characters, of which {1} ({2}%) are 'e'.").format(str(textlen),str(ecount),str((ecount/textlen)*100))
######Pick the Activity#####################
def pick_activity(temp,humid):             #
    if temp == "hot" and humid == "wet":   #  
        print("Watch Netflix")             #
    elif temp == "hot" and humid == "dry": #
        print("Go swimming")               #
    elif temp == "cold" and humid == "wet":#
        print("Go paint")                  #
    else:                                  #
        print("Go read in the cafe")       #
pick_activity("hot","wet")                 #
pick_activity("cold","dry")                #
######Christmas Tree Hashes#################
def space_line(spaces, hashes):
    return spaces * ' ' + hashes * '#'

def triangle(n):
    triangle_str = ''
    for i in range(n):
        triangle_str += (space_line(n-i-1, 2*i+1) + '\n')
    return triangle_str

def main():
    print(triangle(5))

if __name__ == "__main__":
    main()
#####Pos and Neg Number functions#########
def pos_or_neg(num):
    if num > 0:
        print('positive')
    elif num < 0:
        print('negative')

def print_positives(the_ints):
    for num in the_ints:
        if num > 0:
            print(num)

def print_signed_integers(the_ints, the_sign):
    if the_sign == 'positive':
        for i in the_ints:
            if i > 0:
                print(i)
    elif the_sign == 'negative':
        for i in the_ints:
            if i < 0:
                print(i)
########## Wandering Turtle #################
import random
import turtle

def is_in_screen(screen, t):
    left_bound = - screen.window_width() / 2
    right_bound = screen.window_width() / 2
    top_bound = screen.window_height() / 2
    bottom_bound = -screen.window_height() / 2
    turtle_x = t.xcor()
    turtle_y = t.ycor()
    still_in = True
    if turtle_x > right_bound or turtle_x < left_bound:
        still_in = False
    if turtle_y > top_bound or turtle_y < bottom_bound:
        still_in = False
    return still_in

def main():
    julia = turtle.Turtle()
    screen = turtle.Screen()
    julia.shape('turtle')
    while is_in_screen(screen, julia):
        coin = random.randrange(0, 2)
        if coin == 0:
            julia.left(90)
        else:
            julia.right(90)
        julia.forward(50)
    screen.exitonclick()

if __name__ == "__main__":
    main()
############## Flip a Coin   ################

import random
def flip_coin():
    coin = random.randrange(0, 2)
    if coin == 0:
        return "heads"
    else:
        return "tails"
def main():
    keep_playing = input("Would you like to flip a coin? If so, enter Yes.")
    while keep_playing == "Yes":
        print(flip_coin())
        keep_playing = input("Would you like to flip a coin? If so, enter Yes.")
    print("Thanks for flipping!")
if __name__ == "__main__":
    main()

######Chapter 7 Sherlock STUDIO##############
def sherlock(guests):
   for guest in guests:
        if guest == "Inspector Lestrade" and "Dr. Watson":
            return "Enter"
        else:
            return "Go Away! (sound of violin music...)"

def main():
    press = ["a reporter from the Times", "a reporter from the Observer"]
    family_etc = ["Mycroft Holmes", "Mrs. Hudson"]
    enemies = ["Professor Moriarty", "Charles Augustus Milverton", "John Woodley"]
    potential_love_interest = ["Irene Adler"]
    friends = ["Inspector Lestrade", "Dr. Watson"]
    print(sherlock(press))
    print(sherlock(family_etc))
    print(sherlock(enemies))
    print(sherlock(potential_love_interest))
    print(sherlock(friends))

if __name__ == "__main__":
    main()
########## List Methods ###############
.append() Add element to the end of the list
.sort() Order the list. A comparison function may be passed as a parament
.reverse() Reverse the list
.index(x) Return the index of the first occurrence of x
.insert(i,x) Insert x into the i'th index position
.count(x) Return the number of occurrences of x in the list
.remove(x) Delete the first occurrence of x in the list
.pop(i) Delete the i'th element of the list and return its value
###########Data import and sort########
qbfile = open("qbdata.txt", "r")

for aline in qbfile:
    values = aline.split()
    print('QB', values[0], values[1], 'had a rating of', values[10] )

qbfile.close()
###########Data import and sort########
infile = open("qbdata.txt", "r")
line = infile.readline()
while line:
    values = line.split()
    print('QB ', values[0], values[1], 'had a rating of ', values[10] )
    line = infile.readline()

infile.close()
#########Data import and sort##########
>>> infile = open("qbdata.txt", "r")
>>> aline = infile.readline()
>>> aline
'Colt McCoy QB, CLE\t135\t222\t1576\t6\t9\t60.8%\t74.5\n'
>>>
>>> infile = open("qbdata.txt", "r")
>>> linelist = infile.readlines()
>>> print(len(linelist))
34
>>> print(linelist[0:4])
['Colt McCoy QB CLE\t135\t222\t1576\t6\t9\t60.8%\t74.5\n',
 'Josh Freeman QB TB\t291\t474\t3451\t25\t6\t61.4%\t95.9\n',
 'Michael Vick QB PHI\t233\t372\t3018\t21\t6\t62.6%\t100.2\n',
 'Matt Schaub QB HOU\t365\t574\t4370\t24\t12\t63.6%\t92.0\n']
>>>
>>> infile = open("qbdata.txt", "r")
>>> filestring = infile.read()
>>> print(len(filestring))
1708
>>> print(filestring[:256])
Colt McCoy QB CLE   135     222     1576    6       9       60.8%   74.5
Josh Freeman QB TB  291     474     3451    25      6       61.4%   95.9
Michael Vick QB PHI 233     372     3018    21      6       62.6%   100.2
Matt Schaub QB HOU  365     574     4370    24      12      63.6%   92.0
Philip Rivers QB SD 357     541     4710    30      13      66.0%   101.8
Matt Ha
>>>
############  Counting Characters ####################
"""
A program to take grade input and calculate a student's GPA
"""
grades = []
continue_entry = True
# gather grade information
while continue_entry:
    grade = input("Your grade (0.0-4.0): ")
    credits = input("# credits: ")
    # store grades
    grades.append({'grade': float(grade), 'credits': int(credits)})
    user_wants_to_continue = input("Enter another grade? [y/n]: ")
    if user_wants_to_continue == 'n':
        continue_entry = False
# compute GPA
total_quality_score = 0
total_credits = 0
# calculate quality scores and total
for grade_info in grades:
    total_quality_score += (grade_info['grade'] * grade_info['credits'])
    total_credits += grade_info['credits']
gpa = total_quality_score / total_credits
print("Your GPA is:", gpa)
#################  Lists Functions Modifiers Side Effects #
def double_stuff(alist):
    """ Overwrite each element in alist with double its value. """
    for i in range(len(alist)):
        alist[i] = 2 * alist[i]

def main():
    things = [2, 5, 9]
    print(things)
    double_stuff(things)
    print(things)

if __name__ == "__main__":
    main()
#### Lists and Pure Functions with no Side Effects #######
def double_stuff(alist):
    """ Return a new list in which contains doubles of the elements in alist. """
    new_list = []
    for value in alist:
        new_elem = 2 * value
        new_list.append(new_elem)
    return new_list

def main():
    things = [2, 5, 9]
    print(things)
    things = double_stuff(things)
    print(things)

if __name__ == "__main__":
    main()
## Function Pattern to Create and Return a List ##########
initialize a result variable to be an empty list
loop
   create a new element
   append it to result
return the result
 --------or-----------
def primes_up_to(n):
    """ Return a list of all prime numbers less than n. """
    result = []
    for i in range(2, n):
        if is_prime(i):
            result.append(i)
    return result
##### This will print 10 ##########
alist = [4,2,8,6,5]
blist = [num*2 for num in alist if num%2==1]
print(blist)
def count_odd(nums):
    odd_list = 0
    for num in nums:
        if num%2 != 0:
            odd_list += 1
    return (odd_list)

def main():
    nums = [3,5,7,8,9,22,3,4]
    print(count_odd(nums))
    
if __name__ == "__main__":
    main()
###### Chapter 10 Assignment ##############
def get_country_codes(prices):
    new1 = prices.split(", ")
    new_countries = []
    for item in new1:
        new_item = item[0:2]
        new_countries.append(new_item)
    final_countries = ", ".join(new_countries)
    return(final_countries)     
########### Lists and stuff ############
import random
my_list = []
for number in range(100):
    my_list.append(random.randrange(0,1000))
top_num = 0
for i in range(len(my_list)):
    if my_list[i] > top_num:
        top_num = my_list[i]
print(top_num)
####swap##########
(a, b) = (b, a)
####### Dictionary samples ###############
inventory = {'apples': 430, 'bananas': 312, 'oranges': 525, 'pears': 217}

print(list(inventory.values()))
print(list(inventory.items()))

for (k,v) in inventory.items():
    print("Got", k, "that maps to", v)

for k in inventory:
    print("Got", k, "that maps to", inventory[k])

my_dict = {"cat":12, "dog":6, "elephant":23, "bear":20}
key_list = list(my_dict.keys())
key_list.sort()
print(key_list[3])

old_dog = {"chilite":32,"califron":25,"nycyo":19,"nolalolo":38}
dog_list = list(old_dog.values())
dog_list.sort()
print(dog_list)

my_dict = {"cat":12, "dog":6, "elephant":23, "bear":20}
answer = my_dict.get("cat") // my_dict.get("dog")
print(answer)

total = 0
my_dict = {"cat":12, "dog":6, "elephant":23, "bear":20}
for akey in my_dict:
   if len(akey) > 3:
      total = total + my_dict[akey]
print(total)

opposites = {'up': 'down', 'right': 'wrong', 'true': 'false'}
alias = opposites
print(alias is opposites)
alias['right'] = 'left'
print(opposites['right'])
##### Classes and Such #################
class Point():
    def __init__(self):
        self.x = init_x
        self.y = init_y
    def get_x(self):
        return self.x
    def get_y(self):
        return self.y
    def dis_from_origin():
        return)(self.x**2) + (self.y**2))**.5
###### Chapter 12 Classes Assignment ##############    
class Car():
    def __init__(self,gas_level):
        self.gas = gas_level
    def add_gas(self,add_amt):
        self.gas += add_amt
        return self.gas
    def fill_up(self):
        added = 0
        if self.gas < 13:
            added = 13-self.gas
            self.add_gas(added)
        return added

car1 = Car(9.0)
print(car1.add_gas(3))
car2 = Car(14)
print(car2.fill_up())
########## Class stuff and __repr__ ###############
class Fraction:

    def __init__(self, top, bottom):

        self.num = top        # the numerator is on top
        self.den = bottom     # the denominator is on the bottom

    def __repr__(self):
        return str(self.num) + "/" + str(self.den)

    def get_numerator(self):
        return self.num

    def get_denominator(self):
        return self.den

def main():
    myfraction = Fraction(3, 4)
    print(myfraction)

    print(myfraction.get_numerator())
    print(myfraction.get_denominator())

if __name__ == "__main__":
    main()

#################  Standard Exceptions ###################
Language Exceptions	Description
StandardError	Base class for all built-in exceptions except StopIteration and SystemExit.
ImportError	Raised when an import statement fails.
SyntaxError	Raised when there is an error in Python syntax.
IndentationError	Raised when indentation is not specified properly.
NameError	Raised when an identifier is not found in the local or global namespace.
UnboundLocalError	Raised when trying to access a local variable in a function or method but no value has been assigned to it.
TypeError	Raised when an operation or function is attempted that is invalid for the specified data type.
LookupError	Base class for all lookup errors.
IndexError	Raised when an index is not found in a sequence.
KeyError	Raised when the specified key is not found in the dictionary.
ValueError	Raised when the built-in function for a data type has the valid type of arguments, but the arguments have invalid values specified.
RuntimeError	Raised when a generated error does not fall into any category.
MemoryError	Raised when a operation runs our of memory.
RecursionError	Raised when the maximum recursion depth has been exceeded.
SystemError	Raised when the interpreter finds an internal problem, but when this error is encountered the Python interpreter does not exit.
Math Exceptions	Description
ArithmeticError	Base class for all errors that occur for numeric calculation. You know a math error occurred, but you don’t know the specific error.
OverflowError	Raised when a calculation exceeds maximum limit for a numeric type.
FloatingPointError	Raised when a floating point calculation fails.
ZeroDivisionError	Raised when division or modulo by zero takes place for all numeric types.
I/O Exceptions	Description
FileNotFoundError	Raised when a file or directory is requested but doesn’t exist.
IOError	Raised when an input/ output operation fails, such as the print statement or the open() function when trying to open a file that does not exist. Also raised for operating system-related errors.
PermissionError	Raised when trying to run an operation without the adequate access rights.
EOFError	Raised when there is no input from either the raw_input() or input() function and the end of file is reached.
KeyboardInterrupt	Raised when the user interrupts program execution, usually by pressing Ctrl+c.
Other Exceptions	Description
Exception	Base class for all exceptions. This catches most exception messages.
StopIteration	Raised when the next() method of an iterator does not point to any object.
AssertionError	Raised in case of failure of the Assert statement.
SystemExit	Raised when Python interpreter is quit by using the sys.exit() function. If not handled in the code, it causes the interpreter to exit.
OSError	Raises for operating system related errors.
EnvironmentError	Base class for all exceptions that occur outside the Python environment.
AttributeError	Raised in case of failure of an attribute reference or assignment.
NotImplementedError	Raised when an abstract method that needs to be implemented in an inherited class is not actually implemented.
###Exception/Error Chart####
BaseException
 +-- SystemExit
 +-- KeyboardInterrupt
 +-- GeneratorExit
 +-- Exception
      +-- StopIteration
      +-- StopAsyncIteration
      +-- ArithmeticError
      |    +-- FloatingPointError
      |    +-- OverflowError
      |    +-- ZeroDivisionError
      +-- AssertionError
      +-- AttributeError
      +-- BufferError
      +-- EOFError
      +-- ImportError
      +-- LookupError
      |    +-- IndexError
      |    +-- KeyError
      +-- MemoryError
      +-- NameError
      |    +-- UnboundLocalError
      +-- OSError
      |    +-- BlockingIOError
      |    +-- ChildProcessError
      |    +-- ConnectionError
      |    |    +-- BrokenPipeError
      |    |    +-- ConnectionAbortedError
      |    |    +-- ConnectionRefusedError
      |    |    +-- ConnectionResetError
      |    +-- FileExistsError
      |    +-- FileNotFoundError
      |    +-- InterruptedError
      |    +-- IsADirectoryError
      |    +-- NotADirectoryError
      |    +-- PermissionError
      |    +-- ProcessLookupError
      |    +-- TimeoutError
      +-- ReferenceError
      +-- RuntimeError
      |    +-- NotImplementedError
      |    +-- RecursionError
      +-- SyntaxError
      |    +-- IndentationError
      |         +-- TabError
      +-- SystemError
      +-- TypeError
      +-- ValueError
      |    +-- UnicodeError
      |         +-- UnicodeDecodeError
      |         +-- UnicodeEncodeError
      |         +-- UnicodeTranslateError
      +-- Warning
           +-- DeprecationWarning
           +-- PendingDeprecationWarning
           +-- RuntimeWarning
           +-- SyntaxWarning
           +-- UserWarning
           +-- FutureWarning
           +-- ImportWarning
           +-- UnicodeWarning
           +-- BytesWarning
           +-- ResourceWarning
###Simple Exception Program###
def slices_per_person(num_people):
    num_slices = 8
    slices_per_person = num_slices / num_people
    return slices_per_person

def main():
    num_people_str = input("Number of people attending the pizza party: ")
    num_people_int = int(num_people_str)
    try:
        print("Each person gets", slices_per_person(num_people_int), "slices")
    except ZeroDivisionError:
        print("You must enter a number other than zero. Don't make me sad.")
        main()

if __name__ == "__main__":
    main()
#Exceptions with Input Output Data######
try:
    f = open("my_file.txt", "w")
    try:
        f.write("Writing some data to the file")
    finally:
        f.close()
except IOError:
    print("Error: my_file.txt does not exist or it can't be opened for output.")
#### List comprehensions are concise ways to create lists. The general syntax is:
[<expression> for <item> in <sequence> if  <condition>]  ##############
list_a = [34,35,66,74,22]
list_b = [int(item/2) for item in list_a]
print(list_b)
#### Split and Join ##########
words = ["red", "blue", "green"]
glue = ';'
s = glue.join(words)
print(s)
print(words)
print("***".join(words))
print("".join(words))
###### New Initials program  #########
my_name = "Edgar Allan Poe"
name_list = my_name.split()
init = ""
for name in name_list:
    init = init + name[0]
print(init)
### Dictionaries ##################
python_dic = {}
python_dic['function'] = 'Command or Commands in a programming language that perform one or more computing tasks'
python_dic['python'] = ' great programming language to begin with'
python_dic['variable'] = 'A placeholder that can be assigned to a value of various data types'
family_ages = {'lawrence':'46',"joy":"46","taylor":"18"}
print(family_ages)
print(family_ages['taylor'])
########### Dictionary Samples 2 ###################
python_dic = {}
python_dic['function'] = 'Command or Commands in a programming language that perform one or more computing tasks'
python_dic['python'] = ' great programming language to begin with'
python_dic['variable'] = 'A placeholder that can be assigned to a value of various data types'
family_ages = {'lawrence':'46',"joy":"46","taylor":"18"}
print(family_ages)
print(family_ages['taylor'])
del python_dic['variable']
print(python_dic)


my_dictionary = {}
my_dictionary['name1'] = 'Lawrence'
my_dictionary['name2'] = 'Joy'
print(my_dictionary)
del my_dictionary['name2']
print(my_dictionary)
nina_grades = {'grade1':99,'grade2':93,'grade3':101,'grade4':89}
print(nina_grades['grade3'])

inventory = {'apples': 430, 'bananas': 312, 'oranges': 525, 'pears': 217}

print(list(inventory.values()))
print(list(inventory.items()))

for (k,v) in inventory.items():
    print("Got", k, "that maps to", v)

for k in inventory:
    print("Got", k, "that maps to", inventory[k])
################## Dictionary Tuple matrix matrices #############
#tuple assignment
(name,age,sex) = ("Lawrence",46,"male")
print(age)
def circle_info(r):
    c = 2 * 3.14159 * r
    a = 3.14159 * r * r
    return (c,a)
print(circle_info(10))

user_profile = {"name": "lawrence", "sex":46,"ranking":"novice","psswd":"a3j0}df,o."}
print(user_profile["ranking"])

#sparse matrix
absences = {(0,3):1,(2,1):75,(4,3):88}
#row 0 column 3 is 1
#row 2 column 1 is 2
#row 4 column 3 is 88
print(absences.get((4,3)))

matrix = {(0, 3): 1, (2, 1): 2, (4, 3): 3}
print(matrix.get((0,3)))
print(matrix.get((1, 3), 0))
# Create the sparse matrix using a dictionary
for i in range(5):
for j in range(5):
print(matrix.get((i, j), 0), "", end = '')
print()
################### matrix action ########################
matrix = {(0, 3): 1, (2, 1): 2, (4, 3): 3}
print(matrix.get((0,3)))
print(matrix.get((1, 3), 0))
# Create the sparse matrix using a dictionary #########
for i in range(5):
    for j in range(5):
        print(matrix.get((i, j), 0), "", end = '')
    print()
############# enumerate ########################
tennis_champs = ["Serena Williams", "Simona Halep", "Caroline Wozniacki",
    "Angelique Kerber", "Elina Svitolina"]
for idx, item in enumerate(tennis_champs):
      print(idx, item)

list_of_players = ["law","joy","numnu","maalik","taylor","nina"]
for idx,item in enumerate(list_of_players,1):
    print(idx,item)
ranked_playesw = dict(enumerate(list_of_players,1))
print(ranked_playesw)
player_rank = list(enumerate(list_of_players,1))
print(player_rank)
################ tuple samples ######################################
julia = ("Julia", "Roberts", 1967, "Duplicity", 2009, "Actress", "Atlanta, Georgia")
print(julia[2])
print(julia[2:6])
print(len(julia))
julia = julia[:3] + ("Eat Pray Love", 2010) + julia[5:]
print(julia)

def circle_info(r):
    """ Return (circumference, area) of a circle of radius r """
    c = 2 * 3.14159 * r
    a = 3.14159 * r * r
    return (c, a)

print(circle_info(10))
##################################
class Fraction:

    def __init__(self, top, bottom):

        self.num = top        # the numerator is on top
        self.den = bottom     # the denominator is on the bottom

    def __repr__(self):
        return str(self.num) + "/" + str(self.den)

    def get_numerator(self):
        return self.num

    def get_denominator(self):
        return self.den

def main():
    myfraction = Fraction(3, 4)
    print(myfraction)

    print(myfraction.get_numerator())
    print(myfraction.get_denominator())

if __name__ == "__main__":
    main()
################################################
class Chatbot:
    """ An object that can engage in rudimentary conversation with a human. """

    def __init__(self, name):
        self.name = name

    def greeting(self):
        """ Returns the Chatbot's way of introducing itself. """
        return "Hello, my name is " + self.name

    def response(self, prompt_from_human):
        """ Returns the Chatbot's response to something the human said. """
        return "It is very interesting that you say: '" + prompt_from_human + "'"

class BoredChatbot(Chatbot):
    def response(self, prompt_from_human):
        if len(prompt_from_human) > 20:
            return "zzz... Oh excuse me, I dozed off reading your essay."
        else:
            return "It is very interesting that you say: '" + prompt_from_human + "'"
    
# make a chatbot
sally = BoredChatbot("Sally")
# introduce the chatbot and allow the user to say something
human_message = input(sally.greeting())
# respond to the user
print(sally.response(human_message))
Method	Parameters	Description
keys	none	Returns a view of the keys in the dictionary
values	none	Returns a view of the values in the dictionary
items	none	Returns a view of the key-value pairs in the dictionary
get	key	Returns the value associated with key; None otherwise
get	key,alt	Returns the value associated with key; alt otherwise
##############################################################3
####   Start with a working skeleton program and make small incremental changes. At any point, if there is an error, you will know exactly where it is.
###2.Use temporary variables to hold intermediate values so that you can easily inspect and check them.
###3.Once the program is working, you might want to consolidate multiple statements into compound expressions, but only do this if it does not make the program more difficult to read.
########### List Methods ######################################
# Method	Parameters	Result	Description
# append	item	mutator	Adds a new item to the end of a list
# insert	position, item	mutator	Inserts a new item at the position given
# pop	none	hybrid	Removes and returns the last item
# pop	position	hybrid	Removes and returns the item at position
# sort	none	mutator	Modifies a list to be sorted
# reverse	none	mutator	Modifies a list to be in reverse order
# index	item	return idx	Returns the position of first occurrence of item
# count	item	return ct	Returns the number of occurrences of item
# remove	item	mutator	Removes the first occurrence of item
########################################################################
def alphabet_position(letter):
    alphabet="abcdefghijklmnopqrstuvwxyz"
    letter = letter.lower()
    for char in alphabet:
        if char == letter:
            char_pos = (alphabet.index(char))
            return int(char_pos)

def rotate_character(char,rot):
    alphabet="abcdefghijklmnopqrstuvwxyz"
    char = char.lower()
    first_pos = alphabet_position(char)
    new_pos = (first_pos + rot)%26
    new_char = alphabet[new_pos]
    return new_char
 
from helpers import alphabet_position, rotate_character

def encrypt(text,rot):
    new_text = ""
    for each_char in text:
        if each_char.isalpha():
            if each_char.isupper():
                new_text += (rotate_character(each_char,rot)).upper()
            else:
                new_text += rotate_character(each_char,rot)
        else:
            new_text += each_char
    return new_text

def main():
    input_text = input("Type a message:")
    input_rot = int(input("Rotate by:"))
    encrypt(input_text,input_rot)
if __name__ == "__main__":
    main()
 from helpers import alphabet_position, rotate_character

def encrypt(text,rot_word):
    rot_word = rot_word*(len(text))
    new_text = ""  
    index = 0  
    for i in range(len(text)):
        if text[i]==" ":
            new_text += " "
        elif text[i].isalpha():
            if text[i].isupper():
                new_text += (rotate_character(text[i],alphabet_position(rot_word[index]))).upper()
            else:
                new_text += rotate_character(text[i],alphabet_position(rot_word[index]))
            index +=1
        else:
            new_text += text[i]
    return new_text

def main():
    input_text = input("Type a message:")
    input_rot = input("Encryption key:")
    encrypt(input_text,input_rot)
if __name__ == "__main__":
    main()   
